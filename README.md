# Softuni Exam
[Live Demo](https://softuni.webwizards.bg/)


## Zenblog - Custom WordPress Theme and Plugin

Welcome to the repository for Zenblog - a personalized WordPress theme and plugin designed for blogs focusing on minimalism and functionality.

## Description

Zenblog is a WordPress theme and plugin aimed at providing a simple yet powerful platform for bloggers. It's created with an emphasis on clean design, fast performance, and user-friendly customization options. Whether you're a beginner or an experienced blogger, Zenblog offers the tools you need to create a unique and engaging online presence.

## Features

- Minimalistic Design: Clean and clutter-free layout to keep the focus on your content.
- Customization Options: Easily customize colors, fonts, and layout settings to match your brand or personal style.
- Responsive: Ensures your website looks great and functions smoothly on all devices and screen sizes.
- SEO Friendly: Built with best practices in mind to help improve your site's visibility on search engines.
- Optimized for Speed: Lightweight code and optimized assets for fast loading times and improved user experience.
- Built-in Blogging Tools: Includes essential features for blogging such as post formats, categories, tags, and social sharing options.
- Plugin Compatibility: Works seamlessly with popular WordPress plugins for extended functionality.

## Installation

1. Download: Clone the repository or download the latest release ZIP file.
2. Upload: Upload the theme ZIP file to your WordPress site through the admin dashboard or via FTP.
3. Activate: Activate the Zenblog theme in the WordPress admin panel under Appearance > Themes.
4. Install ACF Plugin: Install and activate the [Advanced Custom Fields (ACF)](https://www.advancedcustomfields.com/) plugin for enhanced customizability.
5. Import ACF Fields: Import the provided ACF field export file (located in the `acf-export` directory) to easily set up the necessary custom fields used by Zenblog.
6. Customize: Customize the theme settings and options to suit your preferences through the WordPress Customizer.
7. Install Plugin: Optionally, install and activate the Zenblog plugin for additional features and enhancements.

## Documentation

For detailed instructions on theme installation, customization, and usage, please refer to the [documentation](link_to_documentation_here).

## Support

If you encounter any issues or have questions about Zenblog, feel free to reach out to our support team at [support@example.com](mailto:support@example.com).

## Contributing

We welcome contributions from the community! If you'd like to contribute to Zenblog, please review our [contribution guidelines](link_to_contribution_guidelines_here) and open a pull request with your proposed changes.

## License

Zenblog is licensed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html). You are free to use, modify, and distribute this software under the terms of the license.

Template Name: ZenBlog
Template URL: https://bootstrapmade.com/zenblog-bootstrap-blog-template/
Author: BootstrapMade.com
License: https:///bootstrapmade.com/license/
