<?php
 /*
 * Plugin Name:       Zenblog Plugin
 * Plugin URI:        https://softuni.bg
 * Description:       Our first plugin for the course
 * Version:           1.0.0
 * Requires at least: 5.0
 * Requires PHP:      8.0
 * Author:            Daniel Kolev
 * Author URI:        https://softuni.bg/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI:        https://example.com/my-plugin/
 * Text Domain:       softuni
 * Domain Path:       /languages
 */
if ( ! defined( 'ZENBLOG_PLUGIN_DIR' ) ) {
    define( 'ZENBLOG_PLUGIN_DIR', plugin_dir_path( __FILE__ ) . 'includes' );
}

if ( ! defined( 'ZENBLOG_PLUGIN_INCLUDES_DIR' ) ) {
    define( 'ZENBLOG_PLUGIN_INCLUDES_DIR', plugin_dir_path( __FILE__ ) . 'includes' );
}


if ( ! defined( 'ZENBLOG_PLUGIN_ASSETS_DIR' ) ) {
    define( 'ZENBLOG_PLUGIN_ASSETS_DIR', plugins_url( 'assets', __FILE__ ) );
}

// load our important files
require ZENBLOG_PLUGIN_INCLUDES_DIR . '/functions.php' ;
require ZENBLOG_PLUGIN_INCLUDES_DIR . '/class-events.php' ;