<?php

/** 
 * Load the plugin assets
 */
function zenblog_plugin_enqueue_assets() {

    $args = array();
    wp_enqueue_script(
        'zenblog-assets-plugin',
        ZENBLOG_PLUGIN_ASSETS_DIR . '/js/scripts.js',
        array( 'jquery' ), 
        '1.1', 
        $args
    );

    wp_localize_script( 'zenblog-assets-plugin', 'zenblog_object',
		array( 
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		)
	);


}
add_action( 'wp_enqueue_scripts', 'zenblog_plugin_enqueue_assets' );

/**
 * Function that handles the AJAX call and add a vote to the post
 *
 * @return void
 */
function zenblog_plugin_vote_button() {

    // if ( empty( $_POST['action'] ) ) {
    //     return;
    // }

    $post_id = esc_attr( $_POST['event_id'] );

    $vote_yes = get_post_meta( $post_id, 'vote-yes', true );
    $vote_no  = get_post_meta( $post_id, 'vote-no', true );

    switch ( $_POST['vote_type'] ) {
        case 'yes':
            if ( empty( $vote_yes ) ) {
                $vote_yes = 0;
            }

            $vote_yes += 1;

            update_post_meta( $post_id, 'vote-yes', $vote_yes );
            break;
        case 'no':
            if ( empty( $vote_no ) ) {
                $vote_no = 0;
            }

            $vote_no += 1;

            update_post_meta( $post_id, 'vote-no', $vote_no );
            break;
        
        default:
            # code...
            break;
    }
    
    echo json_encode( array( 'vote_yes' => $vote_yes, 'vote_no' => $vote_no ));
    wp_die();
    
}

add_action( 'wp_ajax_nopriv_cast_vote', 'zenblog_plugin_vote_button' );
add_action( 'wp_ajax_cast_vote', 'zenblog_plugin_vote_button' );

/**
 * Custom Shortcode to show upcoming events
 *
 * @return void
 */
function zenblog_plugin_upcoming_events( $atts ) {

    $shortcode_atts = shortcode_atts(array(
            'events_number' => '',
        ),
        $atts
    );

    $events_number = '5';

    if ( ! empty( $shortcode_atts['events_number'] ) ) {
        $events_number = $shortcode_atts['events_number'];
    }
    
    $output = '';

    $query_args = array(
        'post_type' => 'event',
        'post_status' => 'publish',
        'posts_per_page' => $events_number,
    );

    $events_query = new WP_Query( $query_args );

    if ( $events_query->have_posts() ){
        while ( $events_query->have_posts() ){
            $events_query->the_post();

            $output .= '<div class="event">';
            $output .= '<h3><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h3>';
            $output .= '<p>' . get_the_excerpt() . '</p>';
            $output .= '</div>';
        }
    }

    wp_reset_postdata();


    return $output;
}
add_shortcode( 'upcoming_events', 'zenblog_plugin_upcoming_events' );


function zenblog_plugin_events_exerpt_length( $length ) {
    global $post;
    if ( $post->post_type == 'event' ) {
        return 13;
    }
    return $length;
}
add_filter('excerpt_length', 'zenblog_plugin_events_exerpt_length');
/**
 * Add the top level menu page.
*/
function zenblog_plugin_options_page() {
	add_menu_page(
		'Zenblog Options',
		'Zenblog Options',
		'manage_options',
		'zenblog-options',
		'zenblog_plugin_options_page_html'
	);
}
/*
 * Register our softuni_options_page to the admin_menu action hook.
 */
add_action( 'admin_menu', 'zenblog_plugin_options_page' );

function zenblog_plugin_options_page_html() {
    include ZENBLOG_PLUGIN_INCLUDES_DIR . '/options-page.php';
}
