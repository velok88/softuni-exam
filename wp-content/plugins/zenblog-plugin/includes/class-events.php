<?php
if ( ! class_exists( 'Events_Cpt' ) ) :

class Events_Cpt {

    function __construct() {

        add_action( 'init', array( $this, 'events_cpt') );
        add_action( 'init', array( $this, 'event_category_taxonomy' ) );

        // Register Metaboxes
		add_action( 'add_meta_boxes', array( $this, 'register_event_meta_boxes' ) );

		// Save Metaboxes
		add_action( 'save_post', array( $this, 'save_event_meta'), 10, 2 );
        
    }

    /**
	 * Register our Events Custom Post Type
	 *
	 * @return void
	 */
	public function events_cpt() {
		$labels = array(
			'name'                  => _x( 'Events', 'Post type general name', 'softuni' ),
			'singular_name'         => _x( 'Event', 'Post type singular name', 'softuni' ),
			'menu_name'             => _x( 'Events', 'Admin Menu text', 'softuni' ),
			'name_admin_bar'        => _x( 'Event', 'Add New on Toolbar', 'softuni' ),
			'add_new'               => __( 'Add New', 'softuni' ),
			'add_new_item'          => __( 'Add New Event', 'softuni' ),
			'new_item'              => __( 'New Event', 'softuni' ),
			'edit_item'             => __( 'Edit Event', 'softuni' ),
			'view_item'             => __( 'View Event', 'softuni' ),
			'all_items'             => __( 'All Events', 'softuni' ),
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'revisions',
			),
			'show_in_rest'       => true
		);

		register_post_type( 'event', $args );
	}

    /**
	 * Register our Category taxonomy for our Events CPT
	 *
	 * @return void
	 */
	public function event_category_taxonomy() {
		$labels = array(
			'name'          => 'Categories',
			'singular_name' => 'Category',

		);

		$args = array(
			'labels'       => $labels,
			'show_in_rest' => true,
			'hierarchical' => true,
		);

		register_taxonomy( 'event-category', 'event', $args );
	}

    /**
	 * Register meta box(es).
	 */
	public function register_event_meta_boxes() {
		add_meta_box( 'event-metadata', __( 'Event metadata', 'softuni' ), array( $this, 'event_date_metabox_callback' ), 'event', 'side' );
	}

	/**
	 * Callback function for my Event Metabox
	 *
	 * @return void
	 */
	public function event_date_metabox_callback( $post ) {
        $event_date         = get_post_meta( $post->ID, 'event-date', true );
        $event_end_date     = get_post_meta( $post->ID, 'event-end-date', true );
        $event_location     = get_post_meta( $post->ID, 'event-location', true );		?>
		<div>
            <p><label for="event-date"><?php esc_html_e( 'Event Date:', 'portfolio-institutional-functionality' ); ?></label></p>
            <p><input id="event-date" type="date" name="event-date" value="<?php echo esc_attr( $event_date ); ?>"></p>
            <p><label for="event-end-date"><?php esc_html_e( 'Event End Date:', 'portfolio-institutional-functionality' ); ?></label></p>
            <p><input id="event-end-date" type="date" name="event-end-date" value="<?php echo esc_attr( $event_end_date ); ?>"></p>
		</div>
		<?php
	}

	/**
    * Saves the event metadata.
    *
    * @param int     $post_id The ID of the post which is currently being edited.
    * @param WP_Post $post The post which is currently being edited.	 *
    * @return void
    */
	public function save_event_meta( $post_id, $post ) {

		// Check whether the user is allowed to edit the post or page.
	    if ( ! current_user_can( 'edit_post', $post->ID ) ) {
		    return;
	    }
        // Create an array holding all the metadata fields and values.
        $events_meta = array();

        if ( isset( $_POST['event-date'] ) ) {
            $events_meta['event-date'] = sanitize_text_field( wp_unslash( $_POST['event-date'] ) );
        }
        if ( isset( $_POST['event-end-date'] ) ) {
            $events_meta['event-end-date'] = sanitize_text_field( wp_unslash( $_POST['event-end-date'] ) );
        }

        // Loop through the $meta_values_array and save/edit/delete the post meta.
        foreach ( $events_meta as $key => $value ) {
            if ( 'revision' === $post->post_type ) {
                return; // Don't store custom data twice.
            }
            $value = implode( ',', (array) $value ); // If $value is an array, make it a CSV (unlikely).
            if ( get_post_meta( $post_id, $key, false ) ) { // If the custom field already has a value.
                update_post_meta( $post_id, $key, $value );
            } else { // If the custom field doesn't have a value.
                add_post_meta( $post_id, $key, $value );
            }
            if ( ! $value ) {
                delete_post_meta( $post_id, $key ); // Delete if blank.
            }
        }
	}

}

$events_cpt = new Events_Cpt;
flush_rewrite_rules();

endif;