<?php
$show_events_voting_buttons = get_option( 'show_events_voting_buttons' );

if ( empty( $show_events_voting_buttons) ) {
    $show_events_voting_buttons = 'hide';
}

// Checks if the is submitted
if ( ! empty( $_POST['zenblog_save'] ) && $_POST['zenblog_save'] == 1 ) {
    update_option( 'show_events_voting_buttons', $_POST['show_events_voting_buttons'], false );
    $show_events_voting_buttons = $_POST['show_events_voting_buttons'];
}
?>

<div class="wrap">

    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
    <form action="" method="post">
        <div>
            <label for="show_events_voting_buttons"><?php _e( 'Show/Hide Events Voting Buttons', 'softuni' ); ?></label>
            <select name="show_events_voting_buttons" id="show_events_voting_buttons">
			    <option value="show" <?php echo $show_events_voting_buttons == 'show'? 'selected' : '' ?>>Show</option>
			    <option value="hide" <?php echo $show_events_voting_buttons == 'hide'? 'selected' : '' ?>>Hide</option>
		    </select>
        </div>
        <div>
            <input class="button button-primary" type="submit" value="Update me">
        </div>
        <input type="hidden" name="zenblog_save" value="1">
    </form>
</div>