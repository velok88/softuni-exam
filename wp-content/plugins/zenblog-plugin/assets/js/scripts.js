jQuery( document ).ready(function($) {
    $(".vote").on('click', function(){
        let voteType = $(this).data('type');
        let eventId = $(this).data('id');

        castVote(voteType, eventId);
    })

    function castVote(voteType, eventId){
        let data = {
            'action': 'cast_vote',
            'vote_type': voteType,
            'event_id': eventId
        };
        $.post(zenblog_object.ajax_url, data, function(response) {
            let responseData = JSON.parse(response);
            $('button[data-type="' + voteType + '"][data-id="' + eventId + '"] .result').html(responseData['vote_' + voteType]);
        })
    }
});