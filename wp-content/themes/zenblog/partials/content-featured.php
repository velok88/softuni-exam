<div class="post-entry-1 lg">
    <a href="<?php the_permalink(); ?>">
        <?php 
        if( has_post_thumbnail() ) {
            the_post_thumbnail( 'post-thumbnail', [ 'class' => 'img-fluid', 'title' => get_the_title() ] );
        } else {
            echo '<img src="assets/img/post-landscape-1.jpg" alt="default img" class="img-fluid">';
        }
    ?>
    </a>
    <div class="post-meta">
        <span class="date"><?php the_category(', '); ?></span>
        <span class="mx-1">&bullet;</span>
        <span><?php the_date( 'F j, Y' ) ?></span>
    </div>
    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <p class="mb-4 d-block"><?php the_excerpt(); ?></p>
    <div class="d-flex align-items-center author">
        <div class="photo">
            <?php echo get_avatar(get_the_author_meta('ID') , 96, '', get_the_author_meta( 'display_name', get_the_author_meta('ID') ),['class'=> 'img-fluid ratio-1x1'] ); ?>
        </div>
        <div class="name">
            <h3 class="m-0 p-0"><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a></h3>
        </div>
    </div>
</div>