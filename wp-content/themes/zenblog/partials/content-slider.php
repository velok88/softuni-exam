<div class="swiper-slide">
  <a href="<?php the_permalink(); ?>" class="img-bg d-flex align-items-end" style="background-image: url('<?php the_post_thumbnail_url()?>;">
    <div class="img-bg-inner">
      <h2><?php the_title(); ?></h2>
      <p><?php the_excerpt() ?></p>
    </div>
  </a>
</div>