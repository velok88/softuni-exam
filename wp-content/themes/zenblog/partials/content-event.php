<div class="d-md-flex post-entry-2 half">
    <a href="<?php the_permalink(); ?>" class="me-4 thumbnail">
    <?php 
        if( has_post_thumbnail() ) {
            the_post_thumbnail( 'post-thumbnail', [ 'class' => 'img-fluid', 'title' => get_the_title() ] );
        } else {
            echo '<img src="assets/img/post-landscape-1.jpg" alt="default img" class="img-fluid">';
        }
    ?>
    </a>
    <div>
    <div class="post-meta"><span class="date"><?php the_category(', '); ?></span> <span>Jul 5th '22</span></div>
    <?php 
						$zenblog_event_date     = strtotime( get_post_meta( get_the_ID(), 'event-date', true ) );
						$zenblog_event_end_date = strtotime( get_post_meta( get_the_ID(), 'event-end-date', true ) );

                        // Take location from ACF field meta box.
                        $zenblog_event_location = get_field( 'event_location' );

						?>
    <p><i class="bi bi-calendar3"></i> <?php echo $zenblog_event_date ? esc_html( gmdate( 'j M Y', $zenblog_event_date ) ) : ''; ?>  -  <?php echo $zenblog_event_end_date ? esc_html( gmdate( 'j M Y', $zenblog_event_end_date ) ) : ''; ?>; <i class="bi bi-geo-alt"></i>
 <?php echo esc_html( $zenblog_event_location ); ?></p>
    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <p><?php the_excerpt(); ?></p>
    <div class="d-flex align-items-center author">
        <div class="photo">
            <?php echo get_avatar(get_the_author_meta('ID') , 96, '', get_the_author_meta( 'display_name', get_the_author_meta('ID') ),['class'=> 'img-fluid ratio-1x1'] ); ?>
        </div>
        <div class="name">
        <h3 class="m-0 p-0"><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a></h3>
    
        </div>
    </div>
    </div>
</div>