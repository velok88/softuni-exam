<div class="post-entry-1 border-bottom">
    <div class="post-meta">
        <span class="date"><?php the_category(', '); ?></span>
        <span class="mx-1">&bullet;</span>
        <span><?php the_date( 'F j, Y' ) ?></span></div>
        <h2 class="mb-2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <span class="author mb-3 d-block"><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a></span>
</div>