<?php

add_theme_support( 'post-thumbnails' );

/**
 * Never worry about cache again!
 */
function softunit_assets( $hook ) {



    //<!-- Vendor JS Files -->
    wp_enqueue_script( 'bootstrap-min-js', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.bundle.min.js', array(), '1.0.0', false );
    wp_enqueue_script( 'swiper-bundle.min.js', get_template_directory_uri() . '/assets/vendor/swiper/swiper-bundle.min.js', array(), '1.0.0', false );
    wp_enqueue_script( 'glitghtbox.min.js', get_template_directory_uri() . '/assets/vendor/glightbox/js/glightbox.min.js', array(), '1.0.0', false );
    wp_enqueue_script( 'aos.js', get_template_directory_uri() . '/assets/vendor/aos/aos.js', array(), '1.0.0', false );
    wp_enqueue_script( 'validate.js', get_template_directory_uri() . '/assets/vendor/php-email-form/validate.js', array(), '1.0.0', false );
    
    //<!-- Template Main JS File -->
    wp_enqueue_script( 'main.js', get_template_directory_uri() . '/assets/js/main.js', array(), '1.0.0', false );

    wp_enqueue_style( 'bootstrap.min-css', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css', false, '1.0.0' );
    wp_enqueue_style( 'bootstrap-icons.css', get_template_directory_uri() . '/assets/vendor/bootstrap-icons/bootstrap-icons.css', false, '1.0.0' );
    wp_enqueue_style( 'swiper.css', get_template_directory_uri() . '/assets/vendor/swiper/swiper-bundle.min.css', false, '1.0.0' );
    wp_enqueue_style( 'glightbox.css', get_template_directory_uri() . '/assets/vendor/glightbox/css/glightbox.min.css', false, '1.0.0' );
    wp_enqueue_style( 'variables.css', get_template_directory_uri() . '/assets/css/variables.css', false, '1.0.0' );
    wp_enqueue_style( 'aos.css', get_template_directory_uri() . '/assets/vendor/aos/aos.css', false, '1.0.4' );
    wp_enqueue_style( 'main.css', get_template_directory_uri() . '/assets/css/main.css', false, '1.0.4' );

}
add_action( 'wp_enqueue_scripts', 'softunit_assets' );

/**
 * Register our navigation menus
 *
 * @return void
 */
function softuni_register_nav_menus() {
	register_nav_menus(
		array(
			'primary_menu'      => __( 'Primary Menu', 'softuni' ),
			'popular_services'  => __( 'Popular Services', 'softuni' ),
			'important_links'   => __( 'Important Links', 'softuni' ),
            'latest_services'   => __( 'Latest Services', 'softuni' )
		)
	);
}
add_action( 'after_setup_theme', 'softuni_register_nav_menus' );

/**
 * Our Sidebars here
 *
 * @return void
 */
function softuni_sidebars() {
    register_sidebar(
		array(
			'id'            => 'footer-1',
			'name'          => __( 'Footer 01' ),
			'description'   => __( 'A short description of the sidebar.' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);

    register_sidebar(
		array(
			'id'            => 'footer-2',
			'name'          => __( 'Footer 02' ),
			'description'   => __( 'A short description of the sidebar.' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);

    register_sidebar(
		array(
			'id'            => 'footer-3',
			'name'          => __( 'Footer 03' ),
			'description'   => __( 'A short description of the sidebar.' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);

    register_sidebar(
		array(
			'id'            => 'home-1',
			'name'          => __( 'Home 1' ),
			'description'   => __( 'A short description of the sidebar.' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
}
add_action( 'widgets_init', 'softuni_sidebars' );

