<?php
/**
 * Template Name: Homepage
 */
?>

<?php get_header(); ?>

<!-- ======= Hero Slider Section ======= -->
<section id="hero-slider" class="hero-slider">
  <div class="container-md" data-aos="fade-in">
    <div class="row">
      <div class="col-12">
        <div class="swiper sliderFeaturedPosts">
          <div class="swiper-wrapper">
            <?php 
            $query_args=array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 10,
            );
            $slider_query=new WP_Query($query_args);
            if ($slider_query->have_posts()){
                while ( $slider_query->have_posts() ){
                    $slider_query->the_post();
                    get_template_part('partials/content','slider');
                }
            }
            ?>
          </div>
          <div class="custom-swiper-button-next">
            <span class="bi-chevron-right"></span>
          </div>
          <div class="custom-swiper-button-prev">
            <span class="bi-chevron-left"></span>
          </div>

          <div class="swiper-pagination"></div>
        </div>
      </div>
    </div>
  </div>
</section><!-- End Hero Slider Section -->

<!-- ======= Post Grid Section ======= -->
<section id="posts" class="posts">
  <div class="container" data-aos="fade-up">
    <div class="row g-5">
      <div class="col-lg-4">
      <?php 
            $zenblog_args = array(
              'posts_per_page'      => 1,
              'post_type'           => 'post',
              'post_status'         => 'publish',
              'orderby'             => 'date',
              'order'               => 'DESC',
              'meta_query'          => array(
                array(
                    'key'       => 'featured_posts',
                    'value'     => '1',
                    'compare'   => '=',
                ),
              )
            );

            $zenblog_query = new WP_Query( $zenblog_args );

            if ( $zenblog_query->have_posts() ) :
                while ( $zenblog_query->have_posts() ) : $zenblog_query->the_post();
                    get_template_part( 'partials/content', 'featured' );
                endwhile;
            else : 
                echo '<p>No featured posts found.</p>';
            endif;
            ?>
      </div>
      <div class="col-lg-8">
        <div class="row g-5">
          <div class="col-lg-4 border-start custom-border">
          <?php
              $zenblog_args_2 = array(
                'posts_per_page'      => 3,
                'post_type'           => 'post',
                'post_status'         => 'publish',
                'orderby'             => 'date',
                'order'               => 'DESC',
              );


              $zenblog_query_2 = new WP_Query( $zenblog_args_2 );

            if ( $zenblog_query_2->have_posts() ) :
                while ( $zenblog_query_2->have_posts() ) : $zenblog_query_2->the_post();
                    get_template_part( 'partials/content-article', 'home' );
                endwhile;
            else : 
                echo '<p>No featured posts found.</p>';
            endif;
              ?>
          </div>
          <div class="col-lg-4 border-start custom-border">
          <?php
              $zenblog_args_3 = array(
                'posts_per_page'      => 3,
                'offset'              => 3,
                'post_type'           => 'post',
                'post_status'         => 'publish',
                'orderby'             => 'date',
                'order'               => 'DESC',
              );

              $zenblog_query_3 = new WP_Query( $zenblog_args_3 );

              if ( $zenblog_query_3->have_posts() ) :
                  while ( $zenblog_query_3->have_posts() ) : $zenblog_query_3->the_post();
                      get_template_part( 'partials/content-article', 'home' );
                  endwhile;
              else : 
                  echo '<p>No featured posts found.</p>';
              endif;
              ?>
          </div>

          <!-- Trending Section -->
          <div class="col-lg-4">

            <div class="trending">
              <h3>Upcoming Events</h3>
              <?php if ( is_active_sidebar ( 'home-1' )) {
                get_sidebar ( 'home-1' );
              }
              ?>
            </div>

          </div> <!-- End Trending Section -->
        </div>
      </div>

    </div> <!-- End .row -->
  </div>
</section> <!-- End Post Grid Section -->

<?php get_footer(); ?>